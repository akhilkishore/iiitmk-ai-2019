import gym
import numpy as np

total_episodes = 200

# initialize the environment
envi = gym.make("GuessingGame-v0")
envi.reset()

# set the initial parameters
a = 1000
b = -1000


for episodes in range(total_episodes):
    print("Episode:", episodes)

    # Choosing the value between the ranges
    guessing = (a + b) / 2

    observation, reward, done, info = envi.step(np.array([guessing]))

    # Chaing parameters depending on if the guess is higher or lower
    if observation == 1:
        print(guessing, " is lower than the target")
        b = guessing
    if observation == 2:
        print("Correct Value reached:", guessing)
        break
    if observation == 3:
        print(guessing, " is higher than the target")
        a = guessing

envi.close()
