import random


def agent(board, your_symbol):
    if board[0][0] == board[0][1] != " " and board[0][2] == " ":
        return 0, 2
    elif board[0][0] == board[0][2] != " " and board[0][1] == " ":
        return 0, 1
    elif board[0][1] == board[0][2] != " " and board[0][0] == " ":
        return 0, 0

    elif board[1][0] == board[1][1] != " " and board[1][2] == " ":
        return 1, 2
    elif board[1][0] == board[1][2] != " " and board[1][1] == " ":
        return 1, 1
    elif board[1][1] == board[1][2] != " " and board[1][0] == " ":
        return 1, 0

    elif board[2][0] == board[2][1] != " " and board[2][2] == " ":
        return 2, 2
    elif board[2][0] == board[2][2] != " " and board[2][1] == " ":
        return 2, 1
    elif board[2][1] == board[2][2] != " " and board[2][0] == " ":
        return 2, 0

    elif board[0][0] == board[2][0] != " " and board[1][0] == " ":
        return 1, 0
    elif board[0][0] == board[1][0] != " " and board[2][0] == " ":
        return 2, 0
    elif board[1][0] == board[2][0] != " " and board[0][0] == " ":
        return 0, 0

    elif board[0][1] == board[2][1] != " " and board[1][1] == " ":
        return 1, 1
    elif board[0][1] == board[1][1] != " " and board[2][1] == " ":
        return 2, 1
    elif board[1][1] == board[2][1] != " " and board[0][1] == " ":
        return 0, 1

    elif board[0][2] == board[2][2] != " " and board[1][2] == " ":
        return 1, 2
    elif board[0][2] == board[1][2] != " " and board[2][2] == " ":
        return 2, 2
    elif board[1][2] == board[2][2] != " " and board[0][2] == " ":
        return 0, 2

    elif board[0][0] == board[1][1] != " " and board[2][2] == " ":
        return 2, 2
    elif board[0][0] == board[2][2] != " " and board[1][1] == " ":
        return 1, 1
    elif board[1][1] == board[2][2] != " " and board[0][0] == " ":
        return 0, 0

    elif board[0][2] == board[2][0] != " " and board[1][1] == " ":
        return 1, 1
    elif board[0][2] == board[1][1] != " " and board[2][0] == " ":
        return 2, 0
    elif board[1][1] == board[2][0] != " " and board[0][2] == " ":
        return 0, 2

    valid = [
        (x, y)
        for x, row in enumerate(board)
        for y, cell in enumerate(row)
        if cell == " "
    ]
    return random.choice(valid)
