import re
import os
import bottle
import argparse
import requests
from json import dumps
from datetime import datetime
import arrow

parser = argparse.ArgumentParser()
parser.add_argument("token", action="store", help="Gitlab Token")
parser.add_argument("secret", action="store", help="Callback Secret Token")
parser.add_argument(
    "instructor", action="store", help="User ID of the course instructor"
)
parser.add_argument(
    "-port", action="store", help="Port to run on", default=os.environ.get("PORT", 8080)
)

args = parser.parse_args()
print("Instructor ID:", args.instructor)
args.port = int(args.port)

app = bottle.Bottle()
gitlab_root = "https://gitlab.com/api/v4"
gitlab_kw = {"headers": {"Private-Token": args.token}}
valid_title = re.compile(r"^#(?:\s+)?(\d+)$")


def bot_speak(msg):
    return f"""{msg}
    
This message was written by the Bot: 🤖"""


@app.post("/gitlabhook")
def push_reaction():
    if bottle.request.headers.get("X-Gitlab-Token") == args.secret:
        json = bottle.request.json
        labels = set([i["title"] for i in json["labels"]])
        if json["object_kind"] != "merge_request":
            return bottle.abort(400, "not merge request")
        mr = json["object_attributes"]
        print(mr["iid"], mr["action"], end=" ")
        if mr["action"] != "open":
            return bottle.abort(400, "Not an opening action")
        project_root = f'{gitlab_root}/projects/{json["project"]["id"]}'
        mr_root = f'{project_root}/merge_requests/{mr["iid"]}'
        mr_updates = {"assignee_id": args.instructor}
        if not valid_title.match(mr["title"]):  # invalid title
            print("invalid title", end=" ")
            requests.post(
                f"{mr_root}/notes",
                json={
                    "body": bot_speak(
                        """The title of this merge request is in the incorrect format. If you are attempting to submit a solution to an assignment, the title has to be #<assignment issue number>. Since it does not have a proper title, the instructor will prioritize other merge requests over this until a proper title has been assigned. You can edit the merge request to fix this issue.

                    Please refer to <https://gitlab.com/gitcourses/iiitmk-ai-2019/issues/3> to see further discussion.
                    """
                    )
                },
                **gitlab_kw,
            )
            labels.add("PeerReview")
        else:  # valid title
            print("title ok", end=" ")
            issue_iid = list(valid_title.search(mr["title"]).groups())[0]
            r = requests.get(f"{project_root}/issues/{issue_iid}", **gitlab_kw)
            try:
                due_date = arrow.get(r.json()["due_date"])
            except Exception as e:
                due_date = None
            if due_date is not None and arrow.now() > due_date:  # late submission
                print("Late submission", end=" ")
                requests.post(
                    f"{mr_root}/notes",
                    json={
                        "body": bot_speak(
                            f"The due date for this assignment was {due_date}. Since it's late I'll be closing this."
                        )
                    },
                    **gitlab_kw,
                )
                labels.add("LateWork")
                labels.add("PeerReview")
                if "OnTimeWork" in labels:
                    labels.remove("OnTimeWork")
            else:  # on time submission
                print("On Time", end=" ")
                labels.add("OnTimeWork")
                if "LateWork" in labels:
                    labels.remove("LateWork")
        mr_updates["labels"] = ",".join(labels)
        print(mr_updates)
        requests.put(f"{mr_root}", data=mr_updates, **gitlab_kw)


app.run(port=args.port)
